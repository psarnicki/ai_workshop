from os.path import join
from os.path import dirname
import numpy as np
from common import train_labels_name, train_data_name, test_labels_name,\
    test_data_name, validation_labels_name, validation_data_name

current_path = dirname(__file__)
"""
This module fetches data for neural network training/testing/validation.

Each method returns a tuple of numpy array objects: data_array, data_labels.

    data_array: a 2D numpy array of 1's and 0's. Each row represents one image. Each image has 32 x 32 pixels,
    therefore every row has 1024 elements.

    data_labels: a 1d numpy array. Each row has ONLY ONE digit that stands for image label.
"""
def get_train_data():
    data_array = np.load(join(current_path, train_data_name))
    data_labels = np.load(join(current_path, train_labels_name))

    return data_array, data_labels


def get_test_data():
    data_array = np.load(join(current_path, test_data_name))
    data_labels = np.load(join(current_path, test_labels_name))

    return data_array, data_labels

def get_validation_data():
    data_array = np.load(join(current_path, validation_data_name))
    data_labels = np.load(join(current_path, validation_labels_name))

    return data_array, data_labels