train_data_name = 'train_data.npy'
train_labels_name = 'train_labels.npy'
test_data_name = 'test_data.npy'
test_labels_name = 'test_labels.npy'
validation_data_name = 'validation_data.npy'
validation_labels_name = 'validation_labels.npy'

file_names = [train_data_name, train_labels_name, test_data_name, test_labels_name,
              validation_data_name, validation_labels_name]