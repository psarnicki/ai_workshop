__author__ = 'psarnicki'

from PIL import Image
import numpy as np
from os.path import join, dirname
from random import randint
from common import train_data_name, train_labels_name, test_data_name, test_labels_name
"""
Reconstructs & prints some images to make sure they are packed correctly.
"""

def list2image(image_vector, size=32):

    image = Image.new("RGB", (size, size), "white")
    for index, value in enumerate(image_vector):
        if value == 1:
            image.putpixel((index/32, index%32), 0)

    return image

#Open one of the numpy arrays and fetch some rows.
images_to_fetch = 5
current_path = dirname(__file__)
np_array = np.load(join(current_path, test_data_name))
np_labels = np.load(join(current_path, test_labels_name))


images_num = len(np_labels)
for i in xrange(images_to_fetch):
    x = randint(0, images_num)
    image_vector = np_array[x]
    image = list2image(image_vector)
    print np_labels[x]
    image.show()
